import unittest
import sqlite3,csv,os
from importBdd import *
from funct import howMuchLines

class TestsUnitaires(unittest.TestCase):
    
    def setUp(self):
        self.con = sqlite3.connect('db_test.sqlite3')
        cur = self.con.cursor()
        with open('query_create.sql') as creation:
            to_create = creation.read()
            cur.execute(to_create)
            

    
    def tearDown(self):
        self.con.close()
        os.remove('db_test.sqlite3')

    def tests(self):
        self.assertEqual(howMuchLines('db_test.sqlite3'),0)
        with open("fichier_tests.csv") as csvfile:
            reader = csv.DictReader(csvfile,delimiter=';')
            if len(reader.fieldnames) == 19:
                for ligne in reader: #Pour chaque ligne du fichier csv
                    value = []           
                    for entite in reader.fieldnames:
                        value.append(ligne[entite])
                    
                    condition = isThatInBdd('db_test.sqlite3',ligne['immatriculation '])
                    if not condition:# si none (si la ligne n'existe pas)
                        insertItInBdd('db_test.sqlite3',tuple(value,))
                    else:
                        UpdateLine('db_test.sqlite3',tuple(value),ligne['immatriculation '])
        
        self.assertEqual(howMuchLines('db_test.sqlite3'),1)


