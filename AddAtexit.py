import atexit


def close_connection(connection):
    if connection:
        connection.close()
        
