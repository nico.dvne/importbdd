import sqlite3

def isThatInBdd(bddname,immat):
    connection = sqlite3.Connection(bddname)
    cursor = connection.cursor()
    with open('query_create.sql') as query_create:
        request = query_create.read()
        cursor.execute(request)
        connection.commit()
    
    to_return = cursor.execute("Select * from voiture where immatriculation  = (?)", (immat,)).fetchone() 
    connection.close()
    return to_return

  

def insertItInBdd(bddname,my_tuple):
    con = sqlite3.Connection(bddname)
    cursor = con.cursor()
    with open('query_insert.sql') as query_insert:
        query = query_insert.read()
        cursor.execute(query,my_tuple)
        con.commit()
    con.close()


def UpdateLine(bddname,my_tuple,immatriculation):
    connection = sqlite3.Connection(bddname)
    cursor = connection.cursor()
    m_tuple = my_tuple + (immatriculation,)
    with open('query_update.sql') as query_u:
        q_update = query_u.read()
        cursor.execute(q_update,m_tuple)
        connection.commit()
    connection.close()
