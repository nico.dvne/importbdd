# -*- coding: utf-8 -*-#
from AddAtexit import *
from importBdd import *
from funct import *
import sqlite3,csv,sys,os,logging,atexit

connection = None
atexit.register(close_connection,connection) 


#Initialisation et Configuration du fichier log
logging.basicConfig(filename='logging.log',level=logging.DEBUG,\
      format='%(asctime)s -- %(name)s -- %(levelname)s -- %(message)s')


#Initialisation des arguments a passer au demarrage

args = initArgs()


#Attribution des données dans les variables
fichier_csv = args.fichier_csv
base_de_donnee = args.base_de_donnee

#Verification de l'existance du fichier csv
#Cependant, si celui n'existe pas, fin du programme.
if not os.path.exists(fichier_csv):
    logging.error("Fichier csv non existant")
    sys.exit(-4)

try:
    with open(fichier_csv) as csvfile:
        reader = csv.DictReader(csvfile,delimiter=';')
        if len(reader.fieldnames) == 19:
            for ligne in reader: #Pour chaque ligne du fichier csv
                value = []           
                for entite in reader.fieldnames:
                    value.append(ligne[entite])

                condition = isThatInBdd(base_de_donnee,ligne['immatriculation '])
                if not condition:# si none (si la ligne n'existe pas)
                    insertItInBdd(base_de_donnee,tuple(value,))
                else:                    
                    UpdateLine(base_de_donnee,tuple(value),ligne['immatriculation '])
                
                    
            logging.info("L'import dans la bdd " + base_de_donnee + " s'est bien deroulee")
        else:
            logging.error('Fichier csv non correspondant')  
                
except Exception as e :
    logging.error("Exception lors de l'insertion")
    print(e)
