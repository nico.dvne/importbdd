import argparse,sqlite3

def initArgs():
    parser = argparse.ArgumentParser(description="ImportBdd")
    parser.add_argument('fichier_csv',help='Fichier Csv a enregistrer dans la base')
    parser.add_argument('base_de_donnee',help="Adresse du fichier sqlite3 dans lequel enregistrer le fichier csv")
    return parser.parse_args()


def howMuchLines(db_name):
    con = sqlite3.Connection(db_name)
    cur = con.cursor()
    nombre_ligne = int(cur.execute("Select count(*) from voiture").fetchone()[0])
    con.close()
    return nombre_ligne